# Commit_hg

### Installation

```sh
$ git clone https://gitlab.com/bantonenko/commit_hg.git
$ cd commit_hg/
$ python3 setup.py install
```

### Usage

- Create config file. Example:
    ```
    [params]
    hg_repository_path = "/home/user/.hg"
    hgrc_file_path = "/home/user/.hgrc"
    limit = 5000
    date = "2020-01-01"
    
    [me]
    username = "Test user <test.user@mail.com>"
    auth.username = "test"
    auth.password = "pass"
    
    [[user]]
    username = "Test user <test.user@mail.com>"
    auth.username = "test"
    auth.password = "pass"
    
    [[user]]
    username = "Test2 user2 <test2.user2@mail.com>"
    auth.username = "test2"
    auth.password = "pass2"
    ```
- set config file with command ```$ commit_hg --set-config {config_file_path}```
- run the command in the terminal before the commit ```$ commit_hg```
