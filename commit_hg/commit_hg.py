#!/usr/bin/python3
import subprocess
from collections import Counter
import operator
from termgraph import termgraph as tg
import sys
import toml
import argparse
import configparser
import requests
import json
import base64

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--no-change-user',  help='No chane user', action='store_true')
    parser.add_argument('-с', '--set-config', type=str, help='Path to config file',  required=False)
    parser.add_argument('-m', '--me',  help='No chane user', action='store_true')

    cli_args = parser.parse_args()

    if cli_args.set_config:
        with open(cli_args.set_config, 'r') as f:
            conf_content = f.read()
        with open('.conf', 'w') as f:
            f.write(conf_content)
        sys.exit(0)
    try:
        config = toml.load('.conf')
    except FileNotFoundError:
        sys.stderr.write("Please set config file with option '--set-config'\n\n")
        sys.exit(1)

    if 'params' not in config:
        sys.stderr.write("Please set 'params' in config file\n")
        sys.exit(1)

    hg_repository_path = config["params"]["hg_repository_path"] if config["params"]["hg_repository_path"] else "/"
    hgrc_file_path = config["params"]["hgrc_file_path"] if config["params"]["hgrc_file_path"] else "~/.hgrc"
    hg_limit = config["params"]["limit"] if config["params"]["limit"] else 5000
    hg_date = config["params"]["date"] if config["params"]["date"] else None

    if "gitlab_users" in config:
        if "file_api_url" not in config["gitlab_users"]:
            sys.stdout.write("\nPlease set 'file_api_url' in '[gitlab_user]'.")
            sys.exit(1)
        gitlab_file_api_url = config["gitlab_users"]["file_api_url"]
        gitlab_access_key = config["gitlab_users"]["access_key"]

        resp = requests.get(gitlab_file_api_url, headers={"PRIVATE-TOKEN": gitlab_access_key} if gitlab_access_key else None)
        if resp.status_code != 200:
            sys.stdout.write(
                "\nError getting users file\nUrl: {url}\nStatus code:{code}\nText:{text}\n".format(url=gitlab_file_api_url, code=resp.status_code, text=resp.text))
            sys.exit(1)
        json_resp = json.loads(resp.text)
        data = base64.b64decode(json_resp['content']).decode()
        config.update(toml.loads(data))

    if 'user' not in config or not config['user']:
        sys.stderr.write("Please set users in config file\n")
        sys.exit(1)
    config_users = [user['username'] for user in config['user']]

    command = ['hg', 'log', '--template={author}\n']
    command.extend(['--user=%s' % x for x in config_users])
    command.append('--limit=' + str(hg_limit))
    if hg_date: command.extend(['--date', '>'+hg_date])

    last_commits = subprocess.check_output(command, cwd=hg_repository_path).split(b'\n')[:-1]
    stat = dict(Counter([x.decode().lower() for x in last_commits]))
    [stat.update({x['username'].lower(): 0}) for x in config['user'] if x['username'].lower() not in stat]
    sort_statistics = dict(sorted(stat.items(), key=operator.itemgetter(1))[::-1])
    labels = list(sort_statistics.keys())
    data = [[sort_statistics[x]] for x in sort_statistics]
    normal_data = [[sort_statistics[x]/10] for x in sort_statistics]
    len_categories = 2
    args = {
            'format': '{:<5.2f}', 'suffix': '', 'no_labels': False,
            'vertical': False, 'stacked': True,
            'different_scale': False, 'calendar': False,
            'start_dt': None, 'custom_tick': '', 'delim': '',
            'verbose': False, 'version': False}
    colors = [91, 94]
    tg.stacked_graph(labels, data, normal_data, len_categories, args, colors)

    if not cli_args.no_change_user:
        hgrc_config = configparser.ConfigParser()

        if cli_args.me:
            ui_username = config['me']['username']
            hgrc_config.read(hgrc_file_path)
            hgrc_config.set("ui", "username", config['me']['username'])
            hgrc_config.set('auth', 'foo.username', config['me']['auth']['username'])
            hgrc_config.set('auth', 'foo.password', config['me']['auth']['password'])
        else:
            user_for_change = list(sort_statistics.keys())[-1]
            ui_username = [x for x in [x['username'] for x in config['user']] if x.lower() == user_for_change][0]
            auth_params = [x['auth'] for x in config['user'] if x['username'] == ui_username][0]
            hgrc_config.read(hgrc_file_path)
            hgrc_config.set("ui", "username", ui_username)
            hgrc_config.set('auth', 'foo.username', auth_params['username'])
            hgrc_config.set('auth', 'foo.password', auth_params['password'])

        with open(hgrc_file_path, "w") as config_file:
            hgrc_config.write(config_file)

        sys.stdout.write(
            "\nSuccess change .hgrc file ({file}) to user: \n{user}\n\n".format(file=hgrc_file_path, user=ui_username))

if __name__ == "__main__":
    main()